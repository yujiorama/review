FROM vvakame/review:2.4
RUN apt-get update && \
  apt-get install -y --no-install-recommends \
  build-essential autoconf automake libffi-dev zlib1g-dev ruby-dev && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*
